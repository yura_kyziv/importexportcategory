<?php
declare(strict_types=1);

namespace Mastering\ImportExportCategory\Model\Export;

use Exception;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\ImportExport\Model\Export\Factory as ExportFactory;
use Magento\ImportExport\Model\Export\AbstractEntity;
use Magento\Inventory\Model\ResourceModel\SourceItem;
use Magento\Inventory\Model\ResourceModel\SourceItem\Collection as SourceItemCollection;
use Magento\InventoryImportExport\Model\Export\ColumnProviderInterface;
use Magento\InventoryImportExport\Model\Export\SourceItemCollectionFactoryInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\ImportExport\Model\ResourceModel\CollectionByPagesIteratorFactory;
use Magento\Catalog\Model\ResourceModel\Category\Attribute\CollectionFactory;
use Magento\Catalog\Model\ResourceModel\Category\Collection as CategoryCollection;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Catalog\Api\CategoryListInterface;
use Magento\Catalog\Model\ResourceModel\Category\Attribute\Collection;
use Psr\Log\LoggerInterface;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;

class Category extends \Magento\ImportExport\Model\Export\Entity\AbstractEav
{
    /**
     * @var CollectionFactory
     */
    private CollectionFactory $attributeCollectionProvider;

    /**
     * @var SourceItemCollectionFactoryInterface
     */
    private SourceItemCollectionFactoryInterface $sourceItemCollectionFactory;

    /**
     * @var ColumnProviderInterface
     */
    private ColumnProviderInterface $columnProvider;

    const STORE_ID = 0;

    private CategoryCollectionFactory $categoryCollectionFactory;

    /**
     * @param CollectionFactory $attributeCollectionProvider
     * @param SourceItemCollectionFactoryInterface $sourceItemCollectionFactory
     * @param ColumnProviderInterface $columnProvider
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param CategoryListInterface $categoryList
     * @param CategoryCollection $collectionFactory
     * @param Collection $collection
     * @param LoggerInterface $logger
     * @param CategoryCollectionFactory $categoryCollectionFactory
     */
    public function __construct(
        CollectionFactory          $attributeCollectionProvider,
        SourceItemCollectionFactoryInterface $sourceItemCollectionFactory,
        ColumnProviderInterface              $columnProvider,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        CategoryListInterface $categoryList,
        CategoryCollection $collectionFactory,
        Collection $collection,
        LoggerInterface $logger,
        CategoryCollectionFactory $categoryCollectionFactory
    )
    {
        $this->_entityCollectionFactory = $collectionFactory;
        $this->attributeCollectionProvider = $attributeCollectionProvider;
        $this->sourceItemCollectionFactory = $sourceItemCollectionFactory;
        $this->columnProvider = $columnProvider;
        $this->logger = $logger;
        $searchCriteria = $searchCriteriaBuilder->create();
        $this->categoryList = $categoryList->getList($searchCriteria); // get category by store id
        $this->collection = $collection;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
    }

    /**
     * @inheritdoc
     * @throws \Exception
     */
    public function getAttributeCollection()
    {
        return $this->attributeCollectionProvider->create();
    }

    /**
     * @inheritdoc
     * @throws Exception
     */
    public function export()
    {
        set_time_limit(0);
        try{
            $writer = $this->getWriter();
            $writer->setHeaderCols($this->_getHeaderColumns());

            foreach ($this->_getEntityCollection()->getItems() as $data) {
                $writer->writeRow($data->toArray());
            }

            return $writer->getContents();
        } catch (\Exeption $e){
            $this->logger->error($e->getMessage());
        }

    }

    /**
     * @inheritdoc
     * @throws Exception
     */
    protected function _getHeaderColumns()
    {
        return $this->_getExportAttributeCodes();
    }

    public function exportItem($item)
    {
        try {
            $row = $this->_addAttributeValuesToRow($item);

            $this->getWriter()->writeRow($row);
        }catch (\Exeption $e){
            $this->logger->error($e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function getEntityTypeCode()
    {
        return 'catalog_category';
    }

    /**
     * @inheritdoc
     */
    protected function _getEntityCollection()
    {
        $list = $this->categoryCollectionFactory->create()
            ->addAttributeToSelect('*')
            ->setStoreId(self::STORE_ID);
       return $list;
    }
}
