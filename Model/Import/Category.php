<?php
declare(strict_types=1);
namespace Mastering\ImportExportCategory\Model\Import;

use Exception;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\ImportExport\Helper\Data as ImportHelper;
use Magento\ImportExport\Model\Import;
use Magento\ImportExport\Model\Import\Entity\AbstractEntity;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregatorInterface;
use Magento\ImportExport\Model\ResourceModel\Helper;
use Magento\ImportExport\Model\ResourceModel\Import\Data;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Store\Model\StoreManagerInterface;

class Category extends AbstractEntity
{

    const ENTITY_CODE = 'catalog_category';

    /**
     * If we should check column names
     */
    protected $needColumnCheck = true;

    /**
     * Need to log in import history
     */
    protected $logInHistory = true;

    protected $_dataValidated = true;

    const STORE_ID = 0;

    private array $urlKeys = [];

    /**
     * Valid column names
     */
    protected $validColumnNames = [
        'all_children',
        'available_sort_by',
        'children',
        'children_count',
        'custom_apply_to_products',
        'custom_design',
        'custom_design_from',
        'custom_design_to',
        'custom_layout_update',
        'custom_layout_update_file',
        'custom_use_parent_settings',
        'default_sort_by',
        'description',
        'display_mode',
        'filter_price_range',
        'image',
        'include_in_menu',
        'is_active',
        'is_anchor',
        'landing_page',
        'level',
        'meta_description',
        'meta_keywords',
        'meta_title',
        'name',
        'page_layout',
        'path',
        'path_in_store',
        'position',
        'url_key',
        'url_path'
    ];

    private array $toCreate = [];
    private array $toUpdate = [];
    private array $toSave = [];
    private array $updatedCategories = [];


    /**
     * @var AdapterInterface
     */
    protected AdapterInterface $connection;

    /**
     * @var CategoryRepositoryInterface
     */
    private CategoryRepositoryInterface $categoryRepository;

    /**
     * @var CategoryFactory
     */
    private CategoryFactory $categoryFactory;

    /**
     * @var \Magento\Framework\DataObject[]
     */
    private array $storeCategoryList;

    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;

    /**
     * @var CollectionFactory
     */
    private CollectionFactory $categoryCollectionFactory;

    /**
     * @param JsonHelper $jsonHelper
     * @param ImportHelper $importExportData
     * @param Data $importData
     * @param Helper $resourceHelper
     * @param ProcessingErrorAggregatorInterface $errorAggregator
     * @param CategoryRepositoryInterface $categoryRepository
     * @param CategoryFactory $categoryFactory
     * @param CollectionFactory $categoryCollectionFactory
     * @param StoreManagerInterface $_storeManager
     */
    public function __construct(
        JsonHelper                                   $jsonHelper,
        ImportHelper                                 $importExportData,
        Data                                         $importData,
        Helper                                       $resourceHelper,
        ProcessingErrorAggregatorInterface           $errorAggregator,
        CategoryRepositoryInterface                  $categoryRepository,
        CategoryFactory                              $categoryFactory,
        CollectionFactory                            $categoryCollectionFactory,
        StoreManagerInterface                        $storeManager
    )
    {
        $this->jsonHelper = $jsonHelper;
        $this->_importExportData = $importExportData;
        $this->_resourceHelper = $resourceHelper;
        $this->_dataSourceModel = $importData;
        $this->errorAggregator = $errorAggregator;
        $this->categoryRepository = $categoryRepository;
        $this->categoryFactory = $categoryFactory;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->storeManager = $storeManager;
    }

    /**
     * Entity type code getter.
     *
     * @return string
     */
    public function getEntityTypeCode(): string
    {
        return static::ENTITY_CODE;
    }

    /**
     * Get available columns
     *
     * @return array
     */
    public function getValidColumnNames(): array
    {
        return $this->validColumnNames;
    }

    /**
     * @param $parentUrlPath
     * @param $category
     * @return string
     */
    public function getUrlPath($parentUrlPath, $category): string
    {
        if ($parentUrlPath == '') {
            $validUrlPath = $category->getId();
        } else {
            $validUrlPath = $parentUrlPath . '/' . $category->getId();
        }
        return $validUrlPath;
    }

    /**
     * Row validation
     *
     * @param array $rowData
     * @param int $rowNum
     *
     * @return bool
     */
    public function validateRow(array $rowData, $rowNum): bool
    {
        if (isset($this->_validatedRows[$rowNum])) {
            return !$this->getErrorAggregator()->isRowInvalid($rowNum);
        }

        $this->_validatedRows[$rowNum] = true;
        return !$this->getErrorAggregator()->isRowInvalid($rowNum);
    }

    /**
     * Import data
     *
     * @return bool
     *
     * @throws Exception
     */
    protected function _importData(): bool
    {
        switch ($this->getBehavior()) {
            case Import::BEHAVIOR_DELETE:
                $this->deleteEntity();
                break;
            case Import::BEHAVIOR_REPLACE:
                $this->saveAndReplaceEntity();
                break;
            case Import::BEHAVIOR_APPEND:
                $this->saveAndReplaceEntity();
                break;
        }
        return true;
    }

    /**
     * Delete entities
     *
     * @return bool
     */
    private function deleteEntity(): bool
    {
        $rows = [];
        while ($bunch = $this->_dataSourceModel->getNextBunch()) {
            foreach ($bunch as $rowNum => $rowData) {
                $this->validateRow($rowData, $rowNum);

                if (!$this->getErrorAggregator()->isRowInvalid($rowNum)) {
                    //$rowId = $rowData[static::ENTITY_ID_COLUMN];
                    //$rows[] = $rowId;
                }

                if ($this->getErrorAggregator()->hasToBeTerminated()) {
                    $this->getErrorAggregator()->addRowToSkip($rowNum);
                }
            }
        }

        if ($rows) {
            return $this->deleteEntityFinish(array_unique($rows));
        }
        return false;
    }

    /**
     * Save and replace entities
     *
     * @return void
     */
    private function saveAndReplaceEntity()
    {
        $behavior = $this->getBehavior();
        $rows = [];
        while ($bunch = $this->_dataSourceModel->getNextBunch()) {
            $entityList = [];

            foreach ($bunch as $rowNum => $row) {
                if (!$this->validateRow($row, $rowNum)) {
                    continue;
                }
                if ($this->getErrorAggregator()->hasToBeTerminated()) {
                    $this->getErrorAggregator()->addRowToSkip($rowNum);
                    continue;
                }
                if ($row['url_key'] == null){
                    $category = $this->categoryFactory->create();
                    $this->addToCreate($category, $row);
                } else {
                    $rowId = $row['url_key'];
                    $rows[] = $rowId;
                    $columnValues = [];
                    foreach ($this->getAvailableColumns() as $columnKey) {
                        $columnValues[$columnKey] = $row[$columnKey];
                    }
                    $entityList[$rowId] = $columnValues;
                }
            }
            if (Import::BEHAVIOR_REPLACE === $behavior) {
                if ($rows && $this->deleteEntityFinish(array_unique($rows))) {
                    $this->saveEntityFinish($entityList);
                }
            } elseif (Import::BEHAVIOR_APPEND === $behavior) {
                $this->saveEntityFinish($entityList);
            }
        }
    }

    /**
     * * Save entities
     *
     * @param array $entityData
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function saveEntityFinish(array $entityData): bool
    {
        if ($entityData) {
            foreach ($entityData as $category) {
                $this->getAllCategoryUrlKeys();
                if (in_array($category['url_key'], $this->urlKeys) ) {
                    try {
                        $categoryModel = $this->categoryFactory->create()->setStoreId(self::STORE_ID)->loadByAttribute('url_key', $category['url_key']);
                        $this->foundChangedCategory($categoryModel, $category);
                    } catch (\Exeption $e) {
                        $categoryModel = $this->categoryFactory->create();
                        $this->addToCreate($categoryModel, $category);
                    }
                } else {
                    $categoryModel = $this->categoryFactory->create();
                    $this->addToCreate($categoryModel, $category);
                }
            }

            $this->updateCategory();//update all
            $this->createCategory();//create all
            $this->saveCategory();

            do {
                $this->toSave = [];
                $this->validateAllData();
                $this->saveCategory();
            } while (count($this->toSave) != 0);

            $this->childValidation();
            $this->saveCategory();

            $this->countItemsUpdated = count(array_unique($this->updatedCategories));

            return true;

        }
        return false;
    }

    /**
     * Delete entities
     *
     * @param array $entityIds
     *
     * @return bool
     */
    private function deleteEntityFinish(array $entityIds): bool
    {
        if ($entityIds) {
            foreach ($entityIds as $entityId) {
                try {
                    //$this->storeLocatorRepository->deleteById($entityId);
                } catch (Exception $e) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Get available columns
     *
     * @return array
     */
    private function getAvailableColumns(): array
    {
        return $this->validColumnNames;
    }

    /**
     * @param string $path
     * @return bool
     */
    private function ifRootCategory(string $path): bool
    {
        $paths = explode('/', $path);
        return count($paths) == 1;
    }

    /**
     * @param $categoryModel
     * @param $categoryData
     * @return void
     */
    private function foundChangedCategory($categoryModel, $categoryData): void
    {
        if ($categoryModel->getData('url_key') != $categoryData['url_key']){
            $this->addToCreate($categoryModel, $categoryData);
            return;
        }

        $attributes = ['path', 'url_path', 'is_active','name', 'display_mode', 'position'];
        $addToUpdate = false;
        foreach ($attributes as $attribute){
            if ($categoryModel->getData($attribute) != $categoryData[$attribute]){
                $categoryModel->setData($attribute, $categoryData[$attribute]);
                $addToUpdate = true;
            }
        }

        if ($addToUpdate) {
            $this->addToUpdate($categoryModel);
        }
    }

    /**
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getAllCategoryUrlKeys(): void
    {
        if (!$this->urlKeys) {
            $storeCategoryList = $this->getAllCategoriesFromStore();
            foreach ($storeCategoryList as $category) {
                $this->urlKeys[$category->getEntityId()] = $category->getUrlKey();
            }
        }
    }

    /**
     * @param $data
     * @return void
     */
    private function addToUpdate($category)
    {
        $this->toUpdate[] = $category;
    }

    /**
     * @param $category
     * @param $data
     * @return void
     */
    private function addToCreate($category, $data)
    {
        $this->toCreate[] = [$category, $data];
    }

    /**
     * @param $category
     * @return void
     */
    private function addToSave($category)
    {
        $this->toSave[] = $category;
    }

    /**
     * @return void
     */
    private function updateCategory()
    {
        foreach ($this->toUpdate as $category) {
            try {
                $this->updatedCategories[] = (int)$category->getId();
                $this->addToSave($category);
            } catch (\Exception $exception) {
                continue;
            }
        }
    }

    /**
     * @return void
     */
    private function createCategory()
    {
        foreach ($this->toCreate as $value) {
            $category = $value[0]; //category object;
            $value = $value[1];
            try {
                $category->setName($value['name']);
                $category->setStoreId(self::STORE_ID);

                if (isset($value['is_active'])) {
                    $category->setIsActive($value['is_active']);
                } else {
                    $category->setIsActive(false);
                }

                if (isset($value['url_key'])) {
                    $category->setUrlKey($value['url_key']);
                }

                if (isset($value['url_path'])) {
                    $category->setUrlPath($value['url_path']);
                }

                $this->addToSave($category);
                $this->countItemsCreated += 1;
            } catch (\Exception $exception) {
                continue;
            }
        }
    }

    /**
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function validateAllData()
    {
        $storeCategoryList = $this->getAllCategoriesFromStore();

        $categoryByKey = $this->getCategoriesId($storeCategoryList);

        $rootCategoryId = $this->getRootCategoryId();
        $rootCategoryModel = $this->categoryRepository->get($rootCategoryId, self::STORE_ID);

        foreach ($storeCategoryList as $category) {
            $toSave = false;
            $parentId = (int)$category->getParentId();
            if ($category->getLevel() != 0 ) {
                if ($parentId == 1) {
                    continue;
                }
                $parent = null;
                if (!$this->ifValidCategory($category, $storeCategoryList)) {
                    $categoryUrlPath = $category->getUrlPath();
                    $categoryUrlKey = $category->getUrlKey();

                    $invalidUrl = $categoryUrlPath != $categoryUrlKey;
                    $invalidPath = !$this->ifValidPath($category, $storeCategoryList);

                    if ($invalidUrl || $invalidPath) {
                        $paths = explode('/', $categoryUrlPath);
                        if ( $paths[0] == '') {
                            $category->setParentId($rootCategoryId);
                        } else {
                            array_pop($paths);
                            if (count($paths) == 0) {
                                $category->setParentId($rootCategoryId);
                                $parent = $rootCategoryModel;
                            } else {
                                $categoryParentUrlKey = end($paths);
                                $parent = $categoryByKey[$categoryParentUrlKey];
                                if (is_null($parent)) {
                                    $this->categoryRepository->delete($category);
                                    $this->countItemsDeleted += 1;
                                    continue;
                                }
                                $category->setParentId($parent->getId());
                            }
                            $parentPath = $parent->getPath();
                            $category->setPath($parentPath . '/' . $category->getId());

                            $parentUrlPath = $parent->getUrlPath();
                            if ($parentUrlPath == '') {
                                $urlPath = $category->getUrlKey();
                            } else {
                                $urlPath = $parentUrlPath . '/' . $category->getUrlKey();
                            }
                            $category->setUrlPath($urlPath);
                        }
                    } else {
                        $category->setParentId($rootCategoryId);
                    }
                    $toSave = true;
                }
                $categoryPath = $category->getPath();
                $paths = explode('/', $categoryPath);
                $categoryLevel = $category->getLevel();
                $categoryPathsLevel = count($paths) - 1;
                if($categoryLevel != $categoryPathsLevel) {
                    $category->setLevel($categoryPathsLevel);
                    $toSave = true;
                }
            }
            if ($toSave) {
                $this->updatedCategories[] = (int)$category->getId();
                $this->addToSave($category);
            }
        }
    }

    /**
     * @return void
     */
    private function saveCategory(): void
    {
        foreach ($this->toSave as $category) {
            try {
                $category->setStoreId(self::STORE_ID);
                $category->save();
            } catch (Exception $exception) {
                continue;
            }
        }
    }

    /**
     * @return \Magento\Framework\DataObject[]
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getAllCategoriesFromStore()
    {
        $list = $this->categoryCollectionFactory->create()
            ->addAttributeToSelect('*')
            ->setStoreId(self::STORE_ID)->getItems();

        return $list;
    }

    /**
     * @param $category
     * @param $categoriesIds
     * @return bool
     */
    private function ifValidUrlPath($category, $categoriesIds): bool
    {
        $urlPath = $category->getUrlPath();
        $parentId = $category->getParentId();

        if (array_key_exists($parentId, $categoriesIds)) {
            $parentCategory = $categoriesIds[$parentId];
        } else {
            return false;
        }

        $parentUrlPath = $parentCategory->getUrlPath();
        if ($parentUrlPath == '') {
            $validUrlPath = $category->getUrlKey();
        } else {
            $validUrlPath = $parentUrlPath . '/' . $category->getUrlKey();
        }

        return $urlPath == $validUrlPath;
    }

    /**
     * @param $category
     * @param $categoriesIds
     * @return bool
     */
    private function ifValidPath($category, $categoriesIds): bool
    {
        $path = $category->getPath();
        $parentId = $category->getParentId();

        if (array_key_exists($parentId, $categoriesIds)) {
            $parentCategory = $categoriesIds[$parentId];
        } else {
            return false;
        }

        $parentPath = $parentCategory->getPath();
        $validPath = $this->getUrlPath($parentPath, $category);

        return $path == $validPath;
    }

    /**
     * @param $category
     * @param $categoriesIds
     * @return bool
     */
    private function ifValidCategory($category, $categoriesIds): bool
    {
        $ifValidUrlPath = $this->ifValidUrlPath($category, $categoriesIds);
        $ifValidPath = $this->ifValidPath($category, $categoriesIds);

        return $ifValidPath && $ifValidUrlPath;
    }

    /**
     * @return int|mixed
     */
    private function getRootCategoryId()
    {
        $ids = [];
        foreach ($this->storeManager->getGroups() as $store) {
            $ids[] = $store->getRootCategoryId();
        }
        $rootCategoryId = $ids[0];
        return $rootCategoryId;
    }

    /**
     * @param array $storeCategoryList
     * @return array
     */
    private function getCategoriesId(array $storeCategoryList): array
    {
        $categoryByKey = [];
        foreach ($storeCategoryList as $category) {
            $categoryByKey[$category->getUrlKey()] = $category;
        }
        return $categoryByKey;
    }

    private function childValidation()
    {
        $storeCategoryList = $this->getAllCategoriesFromStore();

        $children = [];
        foreach ($storeCategoryList as $category) {
            $children[$category->getParentId()][(int)$category->getId()] = (int)$category->getId();
        }
        foreach ($storeCategoryList as $category) {
            $toSave = false;
            $categoryId = (int)$category->getId();
            if(array_key_exists($categoryId, $children)){
                $categoryChildren = $this->getValidChildrenString($children[$categoryId]);
                if($category->getChildren() != $categoryChildren){
                    $category->setChildren($categoryChildren);
                    $toSave = true;
                }
            }
            $categoryChildrenCount = $this->getCategoryChildrenCount($categoryId, $children) - 1;
            if($category->getChildrenCount() != $categoryChildrenCount){
                $category->setChildrenCount($categoryChildrenCount);
                $toSave = true;
            }
            if ($toSave) {
                $this->updatedCategories[] = (int)$category->getId();
                $this->addToSave($category);
            }

        }
    }

    /**
     * @param int $categoryId
     * @param array $children
     * @param int $count
     * @return int
     */
    private function getCategoryChildrenCount(int $categoryId, array $children, int $count = 0): int
    {
        if(array_key_exists($categoryId, $children)){
            foreach ($children[$categoryId] as $child) {
                $count = $this->getCategoryChildrenCount($child, $children, $count);
            }
        }
        $count += 1;
        return $count;
    }

    /**
     * @param array $children
     * @return string
     */
    private function getValidChildrenString(array $children): string
    {
        $childrenCount = count($children);
        if ($childrenCount == 0){
            return'';
        } else if($childrenCount == 1){
            return (string)array_pop($children);
        } else {
            $result = array_shift($children);
            foreach ($children as $child) {
                $result .= ',' . $child;
            }
            return $result;
        }
    }
}