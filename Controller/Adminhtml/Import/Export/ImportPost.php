<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Mastering\ImportExportCategory\Controller\Adminhtml\Import\Export;

use Magento\Framework\Controller\ResultFactory;

class ImportPost extends \Magento\TaxImportExport\Controller\Adminhtml\Rate
{
    /**
     * import action from import/export tax
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $importCategoryFile = $this->getRequest()->getFiles('import_rates_file');
        if ($this->getRequest()->isPost() && isset($importCategoryFile['tmp_name'])) {
            try {
                /** @var $importHandler \Magento\TaxImportExport\Model\Rate\CsvImportHandler */
                $importHandler = $this->_objectManager->create(
                    \Mastering\ImportExportCategory\Model\CsvImportHandler::class
                );
                $importHandler->importFromCsvFile($importCategoryFile);

                $this->messageManager->addSuccess(__('The category has been imported.'));
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addError(__('Invalid file upload attempt'));
            }
        } else {
            $this->messageManager->addError(__('Invalid file upload attempt'));
        }
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl($this->_redirect->getRedirectUrl());
        return $resultRedirect;
    }
}
