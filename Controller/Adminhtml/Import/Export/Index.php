<?php
declare(strict_types=1);

namespace Mastering\ImportExportCategory\Controller\Adminhtml\Import\Export;

use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;

class Index extends Action
{
    /**
     * @see _isAllowed()
     */
    public const ADMIN_RESOURCE = 'Magento_Backend::system';

    /**
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        /** @var Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->setActiveMenu('Mastering_ImportExportCategory')
            ->addBreadcrumb(__('Import/Export Category')->render(), __('List')->render());
        $resultPage->getConfig()->getTitle()->prepend(__('Import/Export Category')->render());

        return $resultPage;
    }
}
