<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Mastering\ImportExportCategory\Controller\Adminhtml\Import\Export;


use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\ResponseInterface;

class ExportPost extends \Magento\TaxImportExport\Controller\Adminhtml\Rate
{

    /**
     * Export action from import/export tax
     *
     * @return ResponseInterface
     */
    public function execute()
    {
        $searchCriteria = $this->_objectManager->create(\Magento\Framework\Api\SearchCriteriaBuilder::class);
        $categoryList = $this->_objectManager->create(\Magento\Catalog\Api\CategoryListInterface::class);
        try {
            $searchCriteria = $searchCriteria->create();
            $categoryList = $categoryList->getList($searchCriteria)->getItems();
        } catch (Exception $exception) {
            throw new Exception($exception->getMessage());
        }
        /** start csv content and set template */
        $headers = new \Magento\Framework\DataObject(
            [
                'entity_id' => __('Id'),
                'name' => __('Name'),
                'url_key' => __('Url key'),
                'url_path' => __('Url path'),
            ]
        );
        $template = '"{{entity_id}}","{{name}}","{{url_key}}","{{url_path}}"';
        $content = $headers->toString($template);

        foreach ($categoryList as $category) {
            $row = new \Magento\Framework\DataObject(
                [
                    'entity_id' => $category->getId(),
                    'name' => $category->getName(),
                    'url_key' => $category->getUrlKey(),
                    'url_path' => $category->getUrlPath(),
                ]
            );
            $content .= "\n" . $row->toString($template);
        }
        $fileContent = ['type' => 'string', 'value' => $content, 'rm' => true];

        return $this->fileFactory->create('category.csv', $fileContent, DirectoryList::VAR_DIR);
    }
}
